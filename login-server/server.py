#!/usr/bin/env python3
# _*_ coding: utf-8 _*_

'''
A simple RESTful server, which answers to $HOSTNAME/login with method POST.
A url form encoded payload is to be provided with two mandatory parameters:
username and password.
The only valid couple of username and password are 'richard' and 'secret'.

Try it with:
curl $HOSTNAME:6666/login -X post -d "username=richard&password=secret"
'''

from flask import Flask, request
from flask_restful import Resource, Api, reqparse

PORT = 6666

app = Flask(__name__)
api = Api(app)

class Login(Resource):

  def post(self):
    parser = reqparse.RequestParser()
    parser.add_argument('username', type=str, help='Chosen username')
    parser.add_argument('password', type=str, help='Chosen password')
    args = parser.parse_args(strict=True)

    if args.username and args.password:
      if args.username == 'richard':
        if args.password == 'secret':
          return {
            'message': 'Successfully logged in!',
            'code': 200
          }, 200
        return {
          'message': 'Wrong password, retry.',
          'code': 400
        }, 400
      return {
          'message': 'User does not exist, retry.',
          'code': 400
        }, 400
    return {
      'message': 'Username or password missing.',
      'code': 400
    }, 400

api.add_resource(Login, '/login')

def start():
  app.run(host='0.0.0.0', port=PORT)

if __name__ == '__main__':
  start()
