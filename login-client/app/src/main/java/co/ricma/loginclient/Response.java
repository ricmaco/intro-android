package co.ricma.loginclient;

/**
 * Created by richard on 9/21/16.
 */
public class Response {

    // a skeleton of the response that will be sent when a request is made

    private String message;
    private int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
