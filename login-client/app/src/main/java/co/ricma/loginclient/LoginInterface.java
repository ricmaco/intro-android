package co.ricma.loginclient;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by richard on 9/21/16.
 */
public interface LoginInterface {

    // our rest interface with the server, handles /login with POST method and a url form encoded
    // payload
    @POST("login")
    @FormUrlEncoded
    Call<Response> login(@Field("username") String username, @Field("password") String password);

}
