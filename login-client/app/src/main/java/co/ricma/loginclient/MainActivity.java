package co.ricma.loginclient;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private EditText userET;
    private EditText pswET;
    private Button loginButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // retrieve elements based on id
        userET = (EditText) findViewById(R.id.userET);
        pswET = (EditText) findViewById(R.id.pswET);
        loginButton = (Button) findViewById(R.id.loginButton);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    public void login(View view) {
        // close soft keyboard
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        // retrieve username and password inserted
        String username = userET.getText().toString();
        String password = pswET.getText().toString();

        // if they are not empty
        if (username.length() > 0 && password.length() > 0) {
            // set progessbar to visible
            progressBar.setVisibility(ProgressBar.VISIBLE);

            // use retrofit
            final Retrofit retrofit = new Retrofit.Builder()
                    .client(new OkHttpClient())
                    .baseUrl("http://10.10.10.4:6666/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            LoginInterface api = retrofit.create(LoginInterface.class);

            Call<Response> call = api.login(username, password);
            call.enqueue(new Callback<Response>() {
                // called when a request successfully reaches server (we cannot assume code
                // request is gone well)
                @Override
                public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                    progressBar.setVisibility(ProgressBar.INVISIBLE);

                    String message = "Some error occurred, please retry.";

                    // if code 200
                    if (response.isSuccessful()) {
                        // get message
                        message = response.body().getMessage();

                        // clear both text fields
                        userET.setText("");
                        pswET.setText("");
                    } else { // if code 400
                        // slightly more complicated way of getting error message
                        try {
                            Response res = (Response) retrofit.responseBodyConverter(
                                    Response.class,
                                    Response.class.getAnnotations()
                            ).convert(response.errorBody());
                            message = res.getMessage();
                            // get message
                        } catch (IOException ioe) {}
                    }

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<Response> call, Throwable t) {
                    progressBar.setVisibility(ProgressBar.INVISIBLE);

                    // show directly an error without inspecting
                    Toast.makeText(getApplicationContext(), "Some error occurred, please retry.",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
