package co.ricma.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText userET;
    private TextView resTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // retrieve elements from the view
        userET = (EditText) findViewById(R.id.userET);
        resTV = (TextView) findViewById(R.id.resTV);
    }

    // called when you click the "Greet me" button
    public void greetMe(View view) {
        // if text field is not empty
        if (userET.getText().length() > 0) {
            // populate large label
            resTV.setText(String.format("Hello, %s!", userET.getText()));
            // erase text field
            userET.setText("");
        }
    }
}
