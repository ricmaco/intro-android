package co.ricma.awebpage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private WebView webView;
    private Button backButton;
    private Button fwdButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // retrieve elements from the view
        webView = (WebView) findViewById(R.id.webView);
        backButton = (Button) findViewById(R.id.backButton);
        fwdButton = (Button) findViewById(R.id.fwdButton);

        // give the ability to use webview as browser
        webView.loadUrl("https://www.google.com");
        webView.setWebViewClient(new WebViewClient());
        // enable javascript
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    // called when "Back" button is pressed
    public void goBack(View view) {
        // if client has a history
        if (webView.canGoBack()) {
            webView.goBack();
        }
    }

    // called when "Forward" button is pressed
    public void goFwd(View view) {
        // if client has a fwd history
        if (webView.canGoForward()) {
            webView.goForward();
        }
    }

    // what if the user uses a physical back button or a system back button? we handle it
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // if button pressed is back and client has a back history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        // else if no history or other button handle normally
        return super.onKeyDown(keyCode, event);
    }
}
